package com.example.data.repository

import com.example.data.local.AppDatabase
import com.example.data.local.mapper.toRoom
import com.example.data.local.mapper.toWordInfo
import com.example.data.network.FreeDictionaryDataSource
import com.example.domain.model.WordInfo
import com.example.domain.repository.WordInfoRepository
import javax.inject.Inject

class WordInfoRepositoryImpl @Inject constructor(
    private val database: AppDatabase,
    private val wordDataSource: FreeDictionaryDataSource
) : WordInfoRepository {

    override suspend fun getWordInfo(word: String): List<WordInfo> {
        val wordInfo = database.wordInfoDao().getWordInfo("%$word%")
        return if (wordInfo.isEmpty()) {
            val response = wordDataSource.fetchWordInfo(word)
            val roomResponse = response.map {
                it.toRoom()
            }
            database.wordInfoDao().addWordInfo(roomResponse)
            response
        } else {
            wordInfo.map {
                it.toWordInfo()
            }
        }
    }
}
