package com.example.data.di

import com.example.data.repository.WordInfoRepositoryImpl
import com.example.domain.repository.WordInfoRepository
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@Module
@InstallIn(SingletonComponent::class)
interface RepositoryModule {

    @Binds
    fun bindWordInfoRepository(impl: WordInfoRepositoryImpl): WordInfoRepository
}
