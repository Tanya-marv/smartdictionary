package com.example.data.di

import com.example.data.network.FreeDictionaryDataSource
import com.example.data.network.impl.FreeDictionaryDataSourceImpl
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@Module
@InstallIn(SingletonComponent::class)
interface DataSourceModule {

    @Binds
    fun bindFreeDictionaryDataSource(impl: FreeDictionaryDataSourceImpl): FreeDictionaryDataSource
}
