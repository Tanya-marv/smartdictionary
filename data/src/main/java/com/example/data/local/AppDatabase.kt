package com.example.data.local

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.example.data.local.dao.WordInfoDao
import com.example.data.local.model.WordInfoRoom

@Database(
    entities = [
        WordInfoRoom::class
    ],
    version = 1
)

abstract class AppDatabase : RoomDatabase() {

    abstract fun wordInfoDao(): WordInfoDao

    companion object {
        private const val DB_NAME = "wordInfo_db"
        private var instance: AppDatabase? = null

        fun getInstance(context: Context): AppDatabase {
            return instance ?: synchronized(this) {
                instance ?: createDataBase(context).also { instance = it }
            }
        }

        private fun createDataBase(context: Context): AppDatabase {
            return Room.databaseBuilder(context, AppDatabase::class.java, DB_NAME)
                .build()
        }
    }
}
