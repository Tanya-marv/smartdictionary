package com.example.data.local.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.TypeConverters
import com.example.data.local.converter.MeaningConverter
import com.example.data.network.model.WordInfoResponse

@Entity(tableName = "wordInfo")
@TypeConverters(MeaningConverter::class)
data class WordInfoRoom(
    @PrimaryKey
    @ColumnInfo(name = "word") val word: String,
    @ColumnInfo(name = "phonetic") val phonetic: String?,
    @ColumnInfo(name = "origin") val origin: String?,
    @ColumnInfo(name = "meaning") val meaning: List<WordInfoResponse.MeaningResponse>
)
