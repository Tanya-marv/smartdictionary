package com.example.data.local.converter

import androidx.room.TypeConverter
import com.example.data.network.model.WordInfoResponse
import com.squareup.moshi.Moshi
import com.squareup.moshi.Types
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory

class MeaningConverter {

    private val moshi: Moshi = Moshi.Builder()
        .add(KotlinJsonAdapterFactory())
        .build()

    private val type = Types.newParameterizedType(
        List::class.java,
        WordInfoResponse.MeaningResponse::class.java
    )

    private val jsonAdapter = moshi.adapter<List<WordInfoResponse.MeaningResponse>>(type)

    @TypeConverter
    fun MeaningToString(meaning: List<WordInfoResponse.MeaningResponse>): String {
        return jsonAdapter.toJson(meaning)
    }

    @TypeConverter
    fun StringToMeaning(string: String): List<WordInfoResponse.MeaningResponse> {
        return jsonAdapter.fromJson(string) ?: error("Error")
    }
}
