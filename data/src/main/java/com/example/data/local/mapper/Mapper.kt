package com.example.data.local.mapper

import com.example.data.local.model.WordInfoRoom
import com.example.data.network.mapper.toMeaning
import com.example.data.network.mapper.toMeaningRoom
import com.example.domain.model.WordInfo

fun WordInfo.toRoom(): WordInfoRoom {
    return WordInfoRoom(
        word = word,
        phonetic = phonetic,
        origin = origin,
        meaning = meaning.map {
            it.toMeaningRoom()
        }
    )
}

fun WordInfoRoom.toWordInfo(): WordInfo {
    return WordInfo(
        word = word,
        phonetic = phonetic,
        origin = origin,
        meaning = meaning.map {
            it.toMeaning()
        }
    )
}
