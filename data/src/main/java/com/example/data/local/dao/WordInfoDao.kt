package com.example.data.local.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.data.local.model.WordInfoRoom

@Dao
interface WordInfoDao {

    @Query("SELECT * FROM wordInfo WHERE word LIKE :word")
    suspend fun getWordInfo(word: String): List<WordInfoRoom>

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun addWordInfo(wordInfo: List<WordInfoRoom>)
}
