package com.example.data.network.model

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class WordInfoResponse(

    @Json(name = "word") val word: String,
    @Json(name = "phonetic") val phonetic: String?,
    @Json(name = "origin") val origin: String?,
    @Json(name = "meanings") val meaning: List<MeaningResponse>
) {

    @JsonClass(generateAdapter = true)
    data class MeaningResponse(
        @Json(name = "partOfSpeech") val partOfSpeech: String?,
        @Json(name = "definitions") val definitions: List<DefinitionResponse>
    )

    @JsonClass(generateAdapter = true)
    data class DefinitionResponse(
        @Json(name = "definition") val definition: String,
        @Json(name = "example") val example: String?,
        @Json(name = "synonyms") val synonyms: List<String?>
    )
}
