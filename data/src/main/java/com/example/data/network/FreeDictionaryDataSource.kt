package com.example.data.network

import com.example.domain.model.WordInfo

interface FreeDictionaryDataSource {

    suspend fun fetchWordInfo(
        word: String
    ): List<WordInfo>
}
