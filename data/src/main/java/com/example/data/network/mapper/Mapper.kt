package com.example.data.network.mapper

import com.example.data.network.model.WordInfoResponse
import com.example.domain.model.WordInfo

fun WordInfoResponse.toWordInfo(): WordInfo {
    return WordInfo(
        word = word,
        phonetic = phonetic,
        origin = origin,
        meaning = meaning.map {
            it.toMeaning()
        }
    )
}

fun WordInfoResponse.MeaningResponse.toMeaning(): WordInfo.Meaning {
    return WordInfo.Meaning(
        partOfSpeech = partOfSpeech,
        definitions = definitions.map {
            it.toDefinition()
        }
    )
}

fun WordInfoResponse.DefinitionResponse.toDefinition(): WordInfo.Definition {
    return WordInfo.Definition(
        definition = definition,
        example = example,
        synonyms = synonyms
    )
}

fun WordInfo.Meaning.toMeaningRoom(): WordInfoResponse.MeaningResponse {
    return WordInfoResponse.MeaningResponse(
        partOfSpeech = partOfSpeech,
        definitions = definitions.map {
            it.toDefinitionRoom()
        }
    )
}

fun WordInfo.Definition.toDefinitionRoom(): WordInfoResponse.DefinitionResponse {
    return WordInfoResponse.DefinitionResponse(
        definition = definition,
        example = example,
        synonyms = synonyms
    )
}
