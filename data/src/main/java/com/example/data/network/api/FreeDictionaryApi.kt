package com.example.data.network.api

import com.example.data.network.model.WordInfoResponse
import retrofit2.http.GET
import retrofit2.http.Path

interface FreeDictionaryApi {

    @GET("{word}")
    suspend fun fetchWordInfo(
        @Path("word") word: String
    ): List<WordInfoResponse>
}
