package com.example.data.network.impl

import com.example.data.network.FreeDictionaryDataSource
import com.example.data.network.api.FreeDictionaryApi
import com.example.data.network.mapper.toWordInfo
import com.example.domain.model.WordInfo
import javax.inject.Inject

class FreeDictionaryDataSourceImpl @Inject constructor(
    private val api: FreeDictionaryApi
) : FreeDictionaryDataSource {

    override suspend fun fetchWordInfo(
        word: String
    ): List<WordInfo> {
        return api.fetchWordInfo(word).map {
            it.toWordInfo()
        }
    }
}
