package com.example.domain.repository

import com.example.domain.model.WordInfo

interface WordInfoRepository {

    suspend fun getWordInfo(
        word: String
    ): List<WordInfo>
}
