package com.example.domain.model

data class ApiBuildConfig(
    val baseUrl: String,
)
