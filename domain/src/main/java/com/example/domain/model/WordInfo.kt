package com.example.domain.model

data class WordInfo(
    val word: String,
    val phonetic: String?,
    val origin: String?,
    val meaning: List<Meaning>
) {
    data class Meaning(
        val partOfSpeech: String?,
        val definitions: List<Definition>
    )
    data class Definition(
        val definition: String,
        val example: String?,
        val synonyms: List<String?>
    )
}
