package com.example.smartdictionary.routing

enum class Destination {
    SEARCH, INFO
}
