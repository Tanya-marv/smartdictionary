package com.example.smartdictionary.routing

interface Router {

    fun fromSearchToInfo(word: String)

    fun back(result: String)
}
