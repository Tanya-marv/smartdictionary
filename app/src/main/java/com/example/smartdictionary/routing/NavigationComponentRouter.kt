package com.example.smartdictionary.routing

import androidx.navigation.NavController

class NavigationComponentRouter(
    private val navController: NavController
) : Router {
    override fun fromSearchToInfo(word: String) {
        navController.navigate("${Destination.INFO}/$word")
    }

    override fun back(result: String) {
        navController.previousBackStackEntry
            ?.savedStateHandle
            ?.set("result", result)
        navController.navigateUp()
    }

    private fun NavController.navigate(destination: Destination) {
        navigate(destination.name)
    }
}
