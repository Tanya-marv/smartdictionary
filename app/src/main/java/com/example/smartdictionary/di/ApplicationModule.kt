package com.example.smartdictionary.di

import com.example.domain.model.ApiBuildConfig
import com.example.smartdictionary.BuildConfig
import com.example.smartdictionary.routing.RoutingChannel
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class ApplicationModule {

    @Provides
    @Singleton
    fun provideBuildConfig(): ApiBuildConfig = ApiBuildConfig(
        baseUrl = BuildConfig.FREE_DICTIONARY_API_BASE_URL
    )

    @Provides
    @Singleton
    fun provideRoutingChannel(): RoutingChannel = RoutingChannel()
}
