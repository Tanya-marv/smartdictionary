package com.example.smartdictionary

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.SideEffect
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.graphics.Color
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.NavType
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import androidx.navigation.navArgument
import com.example.smartdictionary.extension.getResult
import com.example.smartdictionary.routing.Destination
import com.example.smartdictionary.routing.NavigationComponentRouter
import com.example.smartdictionary.routing.Router
import com.example.smartdictionary.routing.RoutingChannel
import com.example.smartdictionary.routing.composable
import com.example.smartdictionary.ui.screen.info.WordInfoScreen
import com.example.smartdictionary.ui.screen.info.WordInfoViewModel
import com.example.smartdictionary.ui.screen.search.SearchWordScreen
import com.example.smartdictionary.ui.screen.search.SearchWordViewModel
import com.example.smartdictionary.ui.theme.SmartDictionaryTheme
import com.google.accompanist.systemuicontroller.rememberSystemUiController
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@ExperimentalComposeUiApi
@ExperimentalFoundationApi
@AndroidEntryPoint
class MainActivity : ComponentActivity() {

    @Inject
    lateinit var wordInfoFactory: WordInfoViewModel.WordInfoAssistedFactory
    @Inject
    lateinit var routingChannel: RoutingChannel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            SmartDictionaryTheme {
                val navController = rememberNavController()
                val router: Router = NavigationComponentRouter(navController)
                LaunchedEffect(RoutingChannel::class.simpleName) {
                    routingChannel.collectWithRouter(router)
                }
                val systemUiController = rememberSystemUiController()
                val useDarkIcons = MaterialTheme.colors.isLight
                val backgroundColor = MaterialTheme.colors.background
                Surface(color = backgroundColor) {
                    SideEffect {
                        systemUiController.apply {
                            setStatusBarColor(
                                color = Color.Black,
                                darkIcons = false
                            )
                            setNavigationBarColor(
                                color = backgroundColor,
                                darkIcons = useDarkIcons
                            )
                        }
                    }
                    NavHost(
                        navController = navController,
                        startDestination = Destination.SEARCH.name
                    ) {

                        composable(Destination.SEARCH) {

                            val viewModel = hiltViewModel<SearchWordViewModel>()

                            SearchWordScreen(
                                viewModel = viewModel
                            )

                            navController.getResult(viewModel::onWordSearch)
                        }

                        composable(
                            route = "${Destination.INFO}/{word}",
                            arguments = listOf(
                                navArgument("word") {
                                    type = NavType.StringType
                                }
                            )
                        ) { backStackEntry ->
                            val word = requireNotNull(
                                backStackEntry.arguments?.getString("word")
                            ) {
                                "word must not be null"
                            }

                            val viewModel = viewModel<WordInfoViewModel>(
                                factory = WordInfoViewModel.provideFactory(wordInfoFactory, word)
                            )
                            WordInfoScreen(
                                viewModel = viewModel
                            )
                        }
                    }
                }
            }
        }
    }
}
