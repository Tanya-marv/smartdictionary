package com.example.smartdictionary.ui.screen.search

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalSoftwareKeyboardController
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.unit.dp
import com.example.smartdictionary.R
import com.example.smartdictionary.ui.composables.InputView
import com.example.smartdictionary.ui.composables.SearchActionButton

@ExperimentalComposeUiApi
@Composable
fun SearchWordScreen(
    viewModel: SearchWordViewModel
) {
    Column(
        modifier = Modifier
            .fillMaxWidth()
            .fillMaxHeight()
            .verticalScroll(rememberScrollState()),
        verticalArrangement = Arrangement.SpaceBetween
    ) {

        Column(
            modifier = Modifier.padding(top = 45.dp)
        ) {
            val inputState = viewModel.wordInputState.collectAsState()
            val keyboardController = LocalSoftwareKeyboardController.current

            Text(
                text = stringResource(id = R.string.tv_search_title),
                style = MaterialTheme.typography.h5,
                fontWeight = FontWeight.Bold,
                modifier = Modifier.padding(top = 24.dp, start = 32.dp)
            )

            InputView(
                value = inputState.value,
                onValueChange = viewModel::onWordInputChange,
                type = KeyboardType.Text,
                onDone = { keyboardController?.hide() },
                placeholder = R.string.iv_search_placeholder,
                modifier = Modifier.padding(top = 32.dp, start = 32.dp, end = 32.dp)
            )
        }

        Row(
            modifier = Modifier
                .fillMaxWidth(),
            horizontalArrangement = Arrangement.End
        ) {
            SearchActionButton(
                onClick = (viewModel::navigateToInfo)
            )
        }
    }
}
