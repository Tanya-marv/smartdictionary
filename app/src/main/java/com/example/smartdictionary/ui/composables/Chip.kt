package com.example.smartdictionary.ui.composables

import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.unit.dp
import com.example.smartdictionary.ui.theme.SmartDictionaryTheme

@Composable
fun Chip(
    text: String,
    onClick: (String) -> Unit
) {
    Box(
        modifier = Modifier
            .padding(
                horizontal = 5.dp,
                vertical = 1.dp
            )
            .clip(RoundedCornerShape(50))
            .background(SmartDictionaryTheme.colors.inputText)
            .clickable {
                onClick(text)
            }
    ) {
        Text(
            text = text,
            color = SmartDictionaryTheme.colors.chipView,
            modifier = Modifier.padding(10.dp)
        )
    }
}
