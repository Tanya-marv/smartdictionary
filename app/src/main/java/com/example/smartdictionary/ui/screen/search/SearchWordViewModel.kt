package com.example.smartdictionary.ui.screen.search

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.smartdictionary.routing.RoutingChannel
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.launch

@HiltViewModel
class SearchWordViewModel @Inject constructor(
    private val routingChanel: RoutingChannel
) : ViewModel() {

    val wordInputState = MutableStateFlow("")

    private val searchStateFlow = MutableStateFlow<SearchScreenState>(SearchScreenState.Init)

    fun onWordInputChange(word: String) {
        wordInputState.value = word
    }

    fun navigateToInfo() {
        viewModelScope.launch {
            routingChanel.navigate {
                fromSearchToInfo(wordInputState.value)
            }
        }
    }

    fun onWordSearch(word: String) {
        viewModelScope.launch {
            searchStateFlow.emit(SearchScreenState.Loading)
            searchStateFlow.emit(SearchScreenState.Success(word))
        }
    }
}
