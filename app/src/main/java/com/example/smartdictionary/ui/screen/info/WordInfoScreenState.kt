package com.example.smartdictionary.ui.screen.info

import com.example.domain.model.WordInfo

sealed class WordInfoScreenState {

    object Loading : WordInfoScreenState()

    object Init : WordInfoScreenState()

    data class Success(val info: List<WordInfo>) : WordInfoScreenState()
}
