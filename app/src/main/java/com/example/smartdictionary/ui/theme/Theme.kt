package com.example.smartdictionary.ui.theme

import androidx.compose.foundation.isSystemInDarkTheme
import androidx.compose.material.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.runtime.CompositionLocalProvider
import androidx.compose.runtime.ReadOnlyComposable
import androidx.compose.runtime.remember

@Composable
fun SmartDictionaryTheme(
    darkTheme: Boolean = isSystemInDarkTheme(),
    content: @Composable() () -> Unit
) {
    val colors = if (darkTheme) {
        DarkWordInfoColorPalette
    } else {
        LightWordInfoColorPalette
    }

    val rememberSmartDictionaryColors = remember {
        colors
    }

    CompositionLocalProvider(LocalWordInfoColors provides rememberSmartDictionaryColors) {
        MaterialThemeInternal(darkTheme, content)
    }
}

@Composable
private fun MaterialThemeInternal(
    darkTheme: Boolean = isSystemInDarkTheme(),
    content: @Composable () -> Unit
) {
    val materialColors = if (darkTheme) DarkMaterialColorPalette else LightMaterialColorPalette
    MaterialTheme(
        colors = materialColors,
        typography = Typography,
        shapes = Shapes,
        content = content
    )
}

object SmartDictionaryTheme {

    val colors: WordInfoColors
        @Composable
        @ReadOnlyComposable
        get() = LocalWordInfoColors.current
}
