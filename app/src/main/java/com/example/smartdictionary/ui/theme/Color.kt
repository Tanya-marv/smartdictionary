package com.example.smartdictionary.ui.theme

import androidx.compose.ui.graphics.Color

val LightGrey = Color(0xFFEBECF0)
val LightGrey2 = Color(0xFFd3d3d3)
val DarkGray = Color(0xFF545454)
val DarkGray2 = Color(0xFF2A2A2A)
val BlackAlpha5 = Color(0x0D000000)
val BlackAlpha38 = Color(0x61000000)
val WhiteAlpha5 = Color(0x0DFFFFFF)
val WhiteAlpha38 = Color(0x61FFFFFF)
val BlueDacnis = Color(0xFF3CE0EC)
val MoodyBlue = Color(0xFF7D73C3)
val Watermelon = Color(0xFFFF476F)
