package com.example.smartdictionary.ui.composables

import androidx.annotation.StringRes
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.text.KeyboardActionScope
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.Text
import androidx.compose.material.TextField
import androidx.compose.material.TextFieldDefaults
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.smartdictionary.ui.theme.SmartDictionaryTheme

@Composable
fun InputView(
    value: String,
    onValueChange: (String) -> Unit,
    type: KeyboardType,
    error: String? = null,
    onDone: (KeyboardActionScope.() -> Unit)?,
    @StringRes placeholder: Int,
    modifier: Modifier
) {
    Column {
        TextField(
            value = value,
            onValueChange = onValueChange,
            textStyle = TextStyle(
                fontSize = 18.sp,
                color = SmartDictionaryTheme.colors.inputText
            ),
            colors = TextFieldDefaults.textFieldColors(
                backgroundColor = SmartDictionaryTheme.colors.inputBackground,
                focusedIndicatorColor = Color.Transparent,
                unfocusedIndicatorColor = Color.Transparent
            ),
            isError = error != null,
            placeholder = @Composable {
                Text(
                    text = stringResource(id = placeholder),
                    fontSize = 18.sp,
                    color = SmartDictionaryTheme.colors.inputPlaceholderText
                )
            },
            keyboardOptions = KeyboardOptions(keyboardType = type, imeAction = ImeAction.Done),
            keyboardActions = KeyboardActions(
                onDone = onDone
            ),
            shape = RoundedCornerShape(8.dp),
            modifier = modifier
                .fillMaxWidth()
                .height(60.dp)
        )

        Text(
            text = error ?: "",
            fontSize = 12.sp,
            color = Color.Red,
            modifier = Modifier
                .padding(horizontal = 32.dp, vertical = 8.dp)
        )
    }
}
