package com.example.smartdictionary.ui.screen.search

sealed class SearchScreenState {

    object Loading : SearchScreenState()

    object Init : SearchScreenState()

    data class Success(val word: String) : SearchScreenState()
}
