package com.example.smartdictionary.ui.theme

import androidx.compose.material.darkColors
import androidx.compose.material.lightColors
import androidx.compose.ui.graphics.Color

val LightMaterialColorPalette = lightColors(
    primary = BlueDacnis,
    primaryVariant = BlueDacnis,
    secondary = MoodyBlue,
)

val DarkMaterialColorPalette = darkColors(
    primary = BlueDacnis,
    primaryVariant = BlueDacnis,
    secondary = MoodyBlue
)

val LightWordInfoColorPalette = WordInfoColors(
    inputBackground = BlackAlpha5,
    inputText = Color.Black,
    inputPlaceholderText = BlackAlpha38,
    buttonBackground = BlueDacnis,
    buttonIcon = Color.Black,
    cardView = LightGrey2,
    backCardView = LightGrey,
    chipView = Color.White
)

val DarkWordInfoColorPalette = WordInfoColors(
    inputBackground = WhiteAlpha5,
    inputText = Color.White,
    inputPlaceholderText = WhiteAlpha38,
    buttonBackground = Watermelon,
    buttonIcon = Color.White,
    cardView = DarkGray2,
    backCardView = DarkGray,
    chipView = Color.Black
)
