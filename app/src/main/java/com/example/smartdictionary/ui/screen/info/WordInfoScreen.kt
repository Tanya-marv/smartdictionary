package com.example.smartdictionary.ui.screen.info

import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.background
import androidx.compose.foundation.gestures.LocalOverScrollConfiguration
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material.Card
import androidx.compose.material.CircularProgressIndicator
import androidx.compose.material.Scaffold
import androidx.compose.material.Text
import androidx.compose.material.TopAppBar
import androidx.compose.material.rememberScaffoldState
import androidx.compose.runtime.Composable
import androidx.compose.runtime.CompositionLocalProvider
import androidx.compose.runtime.collectAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.AnnotatedString
import androidx.compose.ui.text.SpanStyle
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.font.FontStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.domain.model.WordInfo
import com.example.smartdictionary.ui.composables.Chip
import com.example.smartdictionary.ui.theme.SmartDictionaryTheme
import com.google.accompanist.flowlayout.FlowRow

@ExperimentalFoundationApi
@Composable
fun WordInfoScreen(
    viewModel: WordInfoViewModel
) {
    when (val state = viewModel.wordInfoStateFlow.collectAsState().value) {
        is WordInfoScreenState.Init -> {
        }
        is WordInfoScreenState.Loading -> {
            Row(
                modifier = Modifier.fillMaxSize(),
                horizontalArrangement = Arrangement.Center,
                verticalAlignment = Alignment.CenterVertically
            ) {
                CircularProgressIndicator(
                    color = Color.Black
                )
            }
        }
        is WordInfoScreenState.Success -> {
            WordInfoView(
                words = state.info,
                onWordCheck = viewModel::onNextWordSearch
            )
        }
    }
}

@ExperimentalFoundationApi
@Composable
fun WordInfoView(
    words: List<WordInfo>,
    onWordCheck: (String) -> Unit
) {

    val scaffoldState = rememberScaffoldState()

    CompositionLocalProvider(
        LocalOverScrollConfiguration provides null
    ) {
        Scaffold(
            scaffoldState = scaffoldState,
            topBar = {
                TopAppBar(
                    backgroundColor = Color.Black,
                    title = {
                        Text(
                            text = words[0].word,
                            color = Color.White,
                            fontSize = 24.sp
                        )
                    }
                )
            },
            modifier = Modifier.fillMaxSize(),
            backgroundColor = Color.LightGray
        ) {

            LazyColumn(
                modifier = Modifier
                    .padding(start = 5.dp, end = 5.dp)
            ) {
                items(words) { word ->
                    Card(
                        modifier = Modifier
                            .fillMaxWidth()
                            .padding(all = 5.dp),
                        elevation = 5.dp,
                        backgroundColor = SmartDictionaryTheme.colors.backCardView
                    ) {
                        Column(
                            modifier = Modifier
                                .padding(all = 10.dp)
                        ) {
                            Text(
                                text = word.word,
                                fontWeight = FontWeight.Bold,
                                fontSize = 18.sp
                            )

                            word.phonetic?.let {
                                Text(
                                    text = "[$it]",
                                    color = Color.Red
                                )
                            }

                            word.origin?.also {
                                Text(
                                    text = it
                                )
                            }

                            word.meaning.forEach { meaning ->
                                Card(
                                    modifier = Modifier
                                        .fillMaxWidth()
                                        .padding(horizontal = 5.dp, vertical = 5.dp),
                                    backgroundColor = SmartDictionaryTheme.colors.cardView
                                ) {
                                    Column(modifier = Modifier.padding(4.dp)) {
                                        val info = meaning.definitions[0]

                                        meaning.partOfSpeech?.also {
                                            Text(
                                                text = it,
                                                fontStyle = FontStyle.Italic,
                                                color = Color.Cyan,
                                                modifier = Modifier
                                                    .padding(start = 15.dp)
                                                    .background(Color.Gray)
                                            )
                                        }

                                        Text(
                                            text = with(AnnotatedString.Builder()) {
                                                pushStyle(
                                                    SpanStyle(
                                                        fontWeight = FontWeight.Bold
                                                    )
                                                )
                                                append("Definition: ")
                                                pop()
                                                append(info.definition)
                                                toAnnotatedString()
                                            },
                                        )

                                        info.example?.also {
                                            Text(
                                                text = with(AnnotatedString.Builder()) {
                                                    pushStyle(
                                                        SpanStyle(
                                                            fontWeight = FontWeight.Bold
                                                        )
                                                    )
                                                    append("Example: ")
                                                    pop()
                                                    append(it)
                                                    toAnnotatedString()
                                                },
                                                fontFamily = FontFamily.Serif,
                                                fontWeight = FontWeight.Normal,
                                                fontStyle = FontStyle.Italic,
                                                modifier = Modifier.padding(top = 10.dp)
                                            )
                                        }

                                        if (info.synonyms.isNotEmpty()) {
                                            FlowRow(
                                                modifier = Modifier.padding(top = 5.dp)
                                            ) {
                                                info.synonyms.forEach {
                                                    it?.also { synonym ->
                                                        Chip(
                                                            text = synonym,
                                                            onClick = {
                                                                onWordCheck(synonym)
                                                            }
                                                        )
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}
