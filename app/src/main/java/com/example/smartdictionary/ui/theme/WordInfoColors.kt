package com.example.smartdictionary.ui.theme

import androidx.compose.runtime.compositionLocalOf
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.structuralEqualityPolicy
import androidx.compose.ui.graphics.Color

val LocalWordInfoColors = compositionLocalOf { LightWordInfoColorPalette }

class WordInfoColors(
    inputBackground: Color,
    inputText: Color,
    chipView: Color,
    inputPlaceholderText: Color,
    buttonBackground: Color,
    buttonIcon: Color,
    cardView: Color,
    backCardView: Color
) {

    val inputBackground by mutableStateOf(inputBackground, structuralEqualityPolicy())
    val inputText by mutableStateOf(inputText, structuralEqualityPolicy())
    val inputPlaceholderText by mutableStateOf(inputPlaceholderText, structuralEqualityPolicy())
    val buttonBackground by mutableStateOf(
        buttonBackground,
        structuralEqualityPolicy()
    )
    val buttonIcon by mutableStateOf(buttonIcon, structuralEqualityPolicy())
    val cardView by mutableStateOf(cardView, structuralEqualityPolicy())
    val backCardView by mutableStateOf(backCardView, structuralEqualityPolicy())
    val chipView by mutableStateOf(chipView, structuralEqualityPolicy())
}
