package com.example.smartdictionary.ui.composables

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.padding
import androidx.compose.material.FloatingActionButton
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.ColorFilter
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import com.example.smartdictionary.R
import com.example.smartdictionary.ui.theme.SmartDictionaryTheme

@Composable
fun SearchActionButton(onClick: () -> Unit = {}) {
    FloatingActionButton(
        onClick = onClick,
        modifier = Modifier
            .padding(bottom = 32.dp, end = 32.dp),
        backgroundColor = SmartDictionaryTheme.colors.buttonBackground
    ) {
        Image(
            painter = painterResource(id = R.drawable.ic_search),
            contentDescription = stringResource(id = R.string.fab_search_cd_search),
            colorFilter = ColorFilter.tint(SmartDictionaryTheme.colors.buttonIcon)
        )
    }
}
