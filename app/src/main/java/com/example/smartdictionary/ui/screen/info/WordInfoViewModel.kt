package com.example.smartdictionary.ui.screen.info

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.viewModelScope
import com.example.domain.repository.WordInfoRepository
import dagger.assisted.Assisted
import dagger.assisted.AssistedFactory
import dagger.assisted.AssistedInject
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch

class WordInfoViewModel @AssistedInject constructor(
    private val wordInfoRepository: WordInfoRepository,
    @Assisted("word") private val word: String
) : ViewModel() {

    private val _wordInfoStateFlow = MutableStateFlow<WordInfoScreenState>(WordInfoScreenState.Init)
    val wordInfoStateFlow = _wordInfoStateFlow.asStateFlow()

    init {
        viewModelScope.launch {
            _wordInfoStateFlow.emit(WordInfoScreenState.Loading)
            val words = wordInfoRepository.getWordInfo(word)
            _wordInfoStateFlow.emit(WordInfoScreenState.Success(words))
        }
    }

    fun onNextWordSearch(word: String) {
        viewModelScope.launch {
            _wordInfoStateFlow.emit(WordInfoScreenState.Loading)
            val nextWord = wordInfoRepository.getWordInfo(word)
            _wordInfoStateFlow.emit(WordInfoScreenState.Success(nextWord))
        }
    }

    @AssistedFactory
    interface WordInfoAssistedFactory {

        fun create(@Assisted("word") word: String): WordInfoViewModel
    }

    companion object {

        fun provideFactory(
            assistedFactory: WordInfoAssistedFactory,
            word: String
        ): ViewModelProvider.Factory = object : ViewModelProvider.Factory {
            @Suppress("UNCHECKED_CAST")
            override fun <T : ViewModel?> create(modelClass: Class<T>): T {
                return assistedFactory.create(word) as T
            }
        }
    }
}
